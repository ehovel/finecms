<?php
if (!defined('IN_FINECMS')) exit();

/**
 * 数据库配置信息
 */
return array(

	'host'     => 'localhost', 
	'username' => 'root', 
	'password' => '', 
	'dbname'   => 'finecmsv1', 
	'prefix'   => 'fn_', 
	'charset'  => 'utf8', 
	'port'     => '', 

);